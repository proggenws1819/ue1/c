public class Human {
    /*string Name
     *Fehler: Datentyp heißt String und nicht string.
     *        Attributnamen sind in lowerCamelCase anzugeben
     *        Semikolon fehlt am Ende
     */
    String name;
    int age;
    /*int height;  /height in centimeter
     *Fehler: Kommentare beginnen mit // nicht /
     */
    int height; //height in cm

    /*public Human(string name, int age, height){
     *Fehler: Datentyp heißt String und nicht string
     *        Fehlender Datentyp für height
     */
    public Human(String name, int age, int height) {
        //Anmerkung: Folgende Zeile wäre ein Fehler gewesen,
        // bevor ich den Attributnamen von Name nach Konvention auf name geändert habe,
        // da ansonsten this.name nicht definiert wäre.
        this.name = name;
        this.age = age;
        this.height = height;
        //Fehlende schließende geschweifte Klammer
    }
}
