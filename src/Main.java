public class Main {
    /*public static void main(String args[]) {
     *Fehler: Parameter der main Methode ist normalerweise String[] args
     */
    public static void main(String[] args) {
        /*Human NewHuman == new human (John, 20, 180);
         *Fehler: Variablennamen werden per Konvention in lowerCamelCase verfasst (NewHuman -> newHuman).
         *        '==' ist ein Vergleichsoperator und keine Zuweisung. '=' ist der Zuweisungsoperator
         *        Die gewünschte Klasse heißt 'Human' und nicht 'human'.
         *        John ist nicht definiert. Da ein String vom Konstrukter von Human erwartet wird,
         *            sollte eigentlich vermutlich der String "John" übergeben werden.
         */
        Human newHuman = new Human("John", 20, 180);
        /*System.out.println(Newhuman.name + " is " NewHuman.age + "years old.);
         *Fehler: Newhuman ist nicht definiert. Vor meiner Änderung war nur NewHuman definiert. Nach Konventionen
         *            sollte es jetzt jedoch newHuman.name heißen
         *        Zwischen " is " und NewHuman.age fehlt ein konkatenierendes +.
         *        NewHuman.age war zuvor kein Fehler. Nach Konventionen sollte es jedoch newHuman.age heißen.
         *        Zwischen " und 'years' fehlt ein Leerzeichen, zwecks Lesbarkeit der Ausgabe.
         *        Nach 'old.' fehlt das schließende ", um den String abzuschließen.
         */
        System.out.println(newHuman.name + " is " + newHuman.age + " years old.");
    }
}
